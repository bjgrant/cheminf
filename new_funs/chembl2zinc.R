chembl2zinc <- function(chemblid, verbose=FALSE) {
  url <- paste("http://zinc.docking.org/catalogs/chembl12/", chemblid, ".zinc", sep="")

  tmpf <- tempfile()
  download.file(url, tmpf, quiet=!verbose)
  lines <- readLines(tmpf)

  if(!length(lines)>0)
    lines <- NULL
  
  return(lines)
}

