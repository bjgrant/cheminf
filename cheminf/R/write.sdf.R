
"write.sdf" <- function(sdf, file="R.sdf", append=TRUE) {
    if(is.sdfs(sdf)) {
        return(write.sdfs(sdf, file=file))
    }
        
  if(!is.sdf(sdf))
    stop("input should be of class 'sdf' as obtained by 'read.sdf'")

  #if(any(is.na(sdf$atom))) {
  #  sdf$atom[ is.na(sdf$atom) ] = 0
  #}
  
  raw.lines <- c()
  raw.lines <- c(raw.lines, sdf$name)
  raw.lines <- c(raw.lines, sdf$id, "")

  fmt <- "%3s%3s%3s%3s%3s%3s%3s%3s%3s %5s %5s"
  sdf$info[is.na(sdf$info)]=" "
  raw.lines <- c(raw.lines, 
             sprintf(fmt, sdf$info[1], sdf$info[2], sdf$info[3],
                     sdf$info[4], sdf$info[5], sdf$info[6],
                     sdf$info[7], sdf$info[8], sdf$info[9],
                     sdf$info[10], sdf$info[11])
                 )
  
  fmt <- "%10s%10s%10s %1s %3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s%3s"
  for ( i in 1:nrow(sdf$atom) ) {
    raw.lines <- c(raw.lines, 
               sprintf(fmt, sdf$atom[i, 1], sdf$atom[i, 2],
                       sdf$atom[i, 3], sdf$atom[i, 4],
                       sdf$atom[i, 5], sdf$atom[i, 6],
                       sdf$atom[i, 7], sdf$atom[i, 8],
                       sdf$atom[i, 9], sdf$atom[i, 10],
                       sdf$atom[i, 10], sdf$atom[i, 12],
                       sdf$atom[i, 13], sdf$atom[i, 14],
                       sdf$atom[i, 15], sdf$atom[i, 16])
                   )
  }

  
  fmt <- "%3s%3s%3s%3s%3s%3s%3s"
  for ( i in 1:nrow(sdf$bond) ) {
    raw.lines <- c(raw.lines, 
                   sprintf(fmt,
                           sdf$bond[i, 1], sdf$bond[i, 2],
                           sdf$bond[i, 3], sdf$bond[i, 4],
                           sdf$bond[i, 5], sdf$bond[i, 6],
                           sdf$bond[i, 7])
                   )
  }
  raw.lines <- c(raw.lines, "M  END")

  if(!is.null(sdf$ass.data)) {
    if(nrow(sdf$ass.data)>0) {
      for ( i in 1:nrow(sdf$ass.data) ) {
        raw.lines <- c(raw.lines, sdf$ass.data[i,1])
        raw.lines <- c(raw.lines, sdf$ass.data[i,2], "")
      }
    }
  }

  raw.lines <- c(raw.lines, "$$$$")
  if(is.null(file)) {
      return(raw.lines)
  }
  else {
      write.table(raw.lines, file = file, quote = FALSE, 
              row.names = FALSE, col.names = FALSE, append = append)
  }
}


write.sdfs <- function(sdfs, file="R.sdf") {

  if(!"sdfs" %in% class(sdfs) & !is.list(sdfs))
      stop("must supply a 'sdfs' object as obtained from read.sdf")
    
  for(i in 1:length(sdfs)) {
    if(i == 1)
      append=FALSE
    else
      append=TRUE

    if(!is.sdf(sdfs[[i]]))
        stop("must supply a 'sdfs' object as obtained from read.sdf")
        
    write.sdf(sdfs[[i]], file=file, append=append)
    
  }
  
}
