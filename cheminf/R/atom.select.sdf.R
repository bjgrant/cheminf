"atom.select.sdf" <- function(sdf, string=NULL,
                              eleno=NULL, elety=NULL,
                              inverse=FALSE,
                              verbose=FALSE, ...) {
  
  str.allowed <- c("noh", "h")
  if(!is.null(string)) {
    if(!(string %in% str.allowed)) {
      stop("Not a valid selection string shortcut.\n\t Please use 'h' or 'noh'\n")
    }
  }
  
  cl <- match.call()
  
  pdb  <- as.pdb.sdf(sdf)
  sele <- bio3d::atom.select.pdb(pdb, string=string, eleno=eleno, elety=elety,
                                 inverse=inverse, verbose=verbose)
  sele$call <- cl

  keep.bonds <- matrix(as.numeric(t(sdf$bond[, c("a","b")])) %in% sele$atom, ncol=2, byrow=T)
  bond.inds  <- which(apply(keep.bonds, 1, all))
  
  sele$bond <- bond.inds
  return(sele)
  
}
