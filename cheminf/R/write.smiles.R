write.smiles <- function(smiles, file=NULL) {
  
  if(is.smiles(smiles) | is.data.frame(smiles)) {
    if(!all(c("smiles", "id") %in% names(smiles)))
      stop("data.frame provided without 'smiles' and 'id' components")
  }
  else {
    if(!is.vector(smiles))
      stop("provide data.frame or a vector of smiles")
    ids <- seq(1, length(smiles))
    
    smiles <- data.frame(cbind(smiles, ids), stringsAsFactors=FALSE)
  }
  
  if(is.null(file))
    file <- "R.can"

  write.table(smiles, file=file, quote=FALSE, sep="\t",
              row.names=FALSE, col.names=FALSE)
  
}
