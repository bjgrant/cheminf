subset.sdfs <- function(x, inds = NULL, ...) {
    
  if(!is.sdfs(x))
    stop("provide a sdfs object as obtained from read.sdf")

  if(is.logical(inds))
      inds <- which(inds)
    
  new <- vector('list', length=length(inds))
  j <- 0
  for(i in inds) {
    j <- j+1
    new[[j]] <- x[[i]]
  }

  if(length(inds)==1)
    new <- new[[1]]
  else
    class(new) <- "sdfs"
  return(new)
}
