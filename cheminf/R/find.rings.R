
.make.graph <- function(sdf) {

  natoms <- nrow(sdf$atom)
  nbonds <- nrow(sdf$bond)
  atom.seq <- seq(1, natoms)
  bonds <- sdf$bond[,1:2]

  vertices <- vector("list", natoms)
  for (i in 1:natoms) {
    a <- unlist(bonds[which(bonds$a==i),])
    b <- unlist(bonds[which(bonds$b==i),])
    adjacent <- sort(unique(c(a,b)))
    adjacent <- adjacent[adjacent!=i]
    vertices[[i]]$adjacent <- adjacent
    vertices[[i]]$discovered <- FALSE
  }

  edges <- vector("list", nbonds)
  for(i in 1:nbonds) {
    a <- as.numeric(bonds[i, 1])
    b <- as.numeric(bonds[i, 2])
    
    edges[[i]] <- list("a"=a, "b"=b,
                       ##dist=1, 
                       back=FALSE,
                       discovered=FALSE)
  }

  out <- list(vertices=vertices, edges=edges)
  return(out)
}


.find.leafs <- function(sdf, verbose=FALSE) {
  natoms <- nrow(sdf$atom)
  nbonds <- nrow(sdf$bond)
  atommap <- seq(1, natoms)
  bondmap <- seq(1, nbonds)

  ## pointers to atom and bond numbers
  sdf$atom <- cbind(sdf$atom, atommap)
  sdf$bond <- cbind(sdf$bond, bondmap)
  
  ## make a non-hydrogen sdf
  inds.h   <- atom.select.sdf(sdf, "h", verbose=verbose)
  inds.noh <- atom.select.sdf(sdf, "noh", verbose=verbose)
  sdf.noh  <- trim.sdf(sdf, inds.noh)
  
  natoms <- nrow(sdf.noh$atom)
  ##bonds <- matrix(as.numeric(sdf.noh$bond[,c("a", "b")]), ncol=2)
  bonds <- sdf.noh$bond[,1:2]

  leaf.atoms <- c()
  for(i in 1:natoms) {

    if(length(which(bonds==i))==1) {
      ##atomnr <- as.numeric(sdf.noh$atom[i, "atommap"])
      atomnr <- sdf.noh$atom[i, "atommap"]
      leaf.atoms <- c(leaf.atoms, atomnr)
    }
  }

  leaf.atoms <- sort(c(leaf.atoms, inds.h$atom))
  return(leaf.atoms)
}


.startdfs <- function(graph, v, u, decreasing=FALSE) {
  cacheEnv <- new.env()
  assign("dfsvertices", graph$vertices, envir = cacheEnv) 
  assign("dfsedges", graph$edges, envir = cacheEnv)

  ##cat("\n v is", v)

  dfs <- function(v, u, decreasing=FALSE, envir=NULL) {
    tmp <- get("dfsvertices", envir = envir)
    tmp[[v]]$discovered <- TRUE
    assign("dfsvertices", tmp, envir = envir)
    
    dfsvertices <- get("dfsvertices", envir = envir)
    dfsedges <- get("dfsedges", envir = envir)
    
    tmp.edgs <- c(which(unlist(lapply(dfsedges, function(x, y) x$a==y, v))),
                  which(unlist(lapply(dfsedges, function(x, y) x$b==y, v))))
    tmp.edgs <- sort(unique(tmp.edgs), decreasing=decreasing)
    if(length(tmp.edgs)>0) {
      for(i in 1:length(tmp.edgs)) {
          e <- dfsedges[[ tmp.edgs[i] ]]
          ##print(e)
        
        if(!e$discovered) {
            tmp <- get("dfsedges", envir = envir)
            tmp[[ tmp.edgs[i] ]]$discovered <- TRUE
            assign("dfsedges", tmp, envir = envir)
          
          if(v==e$a)
            w <- e$b
          else
            w <- e$a
          
          if(!dfsvertices[[w]]$discovered) {
            dfs(w,v,decreasing=decreasing, envir=envir)
          }
          else {
              tmp <- get("dfsedges", envir = envir)
              tmp[[ tmp.edgs[i] ]]$back <- TRUE
              assign("dfsedges", tmp, envir = envir)
          }
        }
      }
    }
  }
  
  dfs(v,u,decreasing=decreasing, envir=cacheEnv)
  out <- list(vertices=get("dfsvertices", envir=cacheEnv),
              edges=get("dfsedges", envir=cacheEnv))

  return(out)
}

.dijkstra <- function(vertices, src, tar) {
   
  natoms <- length(vertices)
  Q <- seq(1, natoms)
  previous <- rep(NA, length=natoms)
  d <- rep(999, length=natoms)
  
  d[src] <- 0
  previous[src] <- 0
  success <- FALSE
  
  while(length(Q) > 0) {
    ## vertex in Q with min dist to src
    tmp <- order(d[sort(Q)])
    u <- Q[tmp[1]]

    #cat("\nu is", u)
    
    ## remove u from Q
    Q <- Q[ -(which(Q %in% u)) ]

    if(length(Q)<1) {
      break;
    }
    
    ## adjacent atoms
    adj <- vertices[[u]]$adjacent
    
    if(any(adj %in% Q)) {
      adj <- adj[ adj %in% Q ]

      for(i in 1:length(adj)) {
        v <- adj[i]
        alt <- d[u] + 1
        if(alt<=d[v]) {
          d[v] <- alt
          previous[v] <- u        

          if(v==tar)
            success <- TRUE
        }
      }
    }
  }

  ## backtrace
  if(success) {
    u <- tar
    s <- c(u)
    while( u != src ) {
      u <- previous[u]
      s <- c(u, s)
    }
    return(s)
  }
  else {
    return(NULL)
  }
}


.find.rings <- function(sdf) {
  graph <- .make.graph(sdf)
  leaf.atoms <- .find.leafs(sdf)

  ## avoid entering leaf atoms
  if(length(leaf.atoms)>0) {
    for(i in 1:length(leaf.atoms)) {
      atomnr <- leaf.atoms[i]
      graph$vertices[[atomnr]]$discovered <- TRUE
      tmp.inds <- which(unlist(lapply(graph$edges, function(x, atomnr) return(x$a==atomnr | x$b==atomnr), atomnr) ))
      for(j in 1:length(tmp.inds)) {
        bondnr <- tmp.inds[j]
        graph$edges[[bondnr]]$discovered <- TRUE
      }
    }
  }
  
  natoms <- nrow(sdf$atom)
  nbonds <- nrow(sdf$bond)
  atom.seq <- seq(1, natoms)
  ##bonds <- matrix(as.numeric(sdf$bond[,1:2]), ncol=2)
  bonds <- sdf$bond[,1:2]
  non.leaf.atoms <- which(!atom.seq %in% leaf.atoms)
  
  back.edges <- c()
  for(i in 1:length(non.leaf.atoms)) {
    j <- non.leaf.atoms[i]
    dec <- FALSE
    newgraph <- .startdfs(graph, j, 0, decreasing=dec)
    back.edges <- c(back.edges,
                    which(unlist(lapply(newgraph$edges, function(x) x$back))))
  }
  
  for(i in 1:length(non.leaf.atoms)) {
    j <- non.leaf.atoms[i]
    dec <- TRUE
    newgraph <- .startdfs(graph, j, 0, decreasing=dec)
    back.edges <- c(back.edges,
                    which(unlist(lapply(newgraph$edges, function(x) x$back))))
  }

  cycle.atoms <- c()
  back.edges <- sort(unique(back.edges))
  if(length(back.edges)>0) {
    for(i in 1:length(back.edges)) {
      e <- back.edges[i]
      
      cycle.atoms <- c(cycle.atoms,
                       graph$edges[[e]]$a,
                       graph$edges[[e]]$b)
      
    }
    cycle.atoms <- unique(sort(cycle.atoms))
  }


  rings <- list()
  if(length(cycle.atoms)>0) {
    for(i in 1:(length(cycle.atoms)-1)) {
      vertices <- graph$vertices
      
      src <- cycle.atoms[i]
      adj.atoms <- vertices[[src]]$adjacent[ which(vertices[[src]]$adjacent %in% cycle.atoms) ]
      
      for(j in 1:length(adj.atoms)) {
        vertices <- graph$vertices
        tar <- adj.atoms[j]
        
        ## remove bond connecting src and tar
        vertices[[src]]$adjacent %in% tar
        vertices[[src]]$adjacent <- vertices[[src]]$adjacent[ which(!(vertices[[src]]$adjacent %in% tar)) ]
        vertices[[tar]]$adjacent <- vertices[[tar]]$adjacent[ which(!(vertices[[tar]]$adjacent %in% src)) ]
        
        vertices[[src]]$adjacent <- vertices[[src]]$adjacent[ vertices[[src]]$adjacent %in% cycle.atoms ]
        vertices[[tar]]$adjacent <- vertices[[tar]]$adjacent[ vertices[[tar]]$adjacent %in% cycle.atoms ]
        
        fastest <- .dijkstra(vertices, src, tar)
        
        if(!is.null(fastest)) {
          j <- length(rings)+1
          rings[[j]] <- sort(fastest)
        }
      }
    }
  }

  if(length(rings)>0) {
    unq.rings <- unique(unlist(lapply(rings, paste, collapse="-")))
    rings <- lapply(strsplit(unq.rings, "-"), as.numeric)

    ## find edges for each ring
    bonds <- list()
    for(i in 1:length(rings)) {
      ri <- rings[[i]]
      bond.is.ring <- apply(sdf$bond, 1, function(x) {
        all(x[1:2] %in% rings[[i]])
      })
      
      bonds[[i]] <- which(bond.is.ring)
    }
    
    out <- list(rings=rings, backedges=back.edges, bonds=bonds)
    return(out)
  }
  else {
    return(NULL)
  }
}
