
chembl.filter.compounds <- function(compounds, type="IC50", range=NULL, decreasing=FALSE) {

  excl.inds <- union(which(compounds[,"value"]=="Unspecified"),
                     which(compounds[,"units"]=="Unspecified"))

  cat(paste("  ", length(excl.inds), " records omitted\n", sep=""))

  new.stuff <- compounds[-excl.inds, ]
  inds <- which(new.stuff[,"bioactivity__type"]==type)
  new.stuff <- new.stuff[inds,]

  
  vals      <- as.numeric(new.stuff[, "value"])
  inds      <- order(vals, decreasing=decreasing)
  new.stuff <- new.stuff[inds, ]
  
  if(is.null(range)) {
    return(new.stuff)
  }
  
  else {
    if(length(range)!=2)
      stop("range should be of length = 2")
    
    vals      <- as.numeric(new.stuff[, "value"])
    inds <- intersect(which(vals > range[1]),
                      which(vals < range[2]))
    return(new.stuff[inds, ])

  }
  
}
  
