"read.smiles" <- function (file) {
  trim <- function(s) {
    s <- sub("^ +", "", s)
    s <- sub(" +$", "", s)
    s[(s == "")] <- NA
    s
  }
  
  if (missing(file))
    stop("read.smiles: please specify a file for reading")
  
  ## Check if file exists localy or online
  toread <- file.exists(file)
  if(substr(file,1,4)=="http") {
    toread <- TRUE
  }
  
  if (!toread) {
    stop("No input SDF file found: check filename")
  }

  ## parse lines
  lines <- readLines(file)
  lines <- gsub("\t", " ", lines)
  lines <- trim(lines)

  ## in case of empty lines
  lines[lines==""] <- NA
  lines <- lines[!is.na(lines)]

  ## split lines
  l <- strsplit(lines, " ")
  lens <- unlist(lapply(l, length))

  if(any(lens>2))
    stop("file format error. possible not a smiles file?")
  
  if(length(unique(lens))>1) {
    inds <- which(lens==1)
    for(i in 1:length(inds)) {
      j <- inds[i]
      l[[j]] <- c(l[[j]], NA)
    }
  }

  lens <- unlist(lapply(l, length))
  m <- matrix(unlist(l), nrow=length(l), byrow=TRUE)

  if(ncol(m)==1)
    m <- cbind(m, NA)
  
  df <- data.frame(m, stringsAsFactors=FALSE)
  colnames(df) <- c("smiles", "id")
  class(df) <- c("data.frame", "smiles")
  return(df)
}
