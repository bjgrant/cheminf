.chembltype <- function(id) {
  url = paste('https://www.ebi.ac.uk/chembl/compound/inspect/', id, sep='')
  if(RCurl::url.exists(url))
    return("compound")
  
  url = paste('https://www.ebi.ac.uk/chembl/target/inspect/', id, sep='')
  if(RCurl::url.exists(url))
    return("target")
  
  url = paste('https://www.ebi.ac.uk/chembl/assay/inspect/', id, sep='')
  if(RCurl::url.exists(url))
    return("assay")

  stop(paste("url (", url, ") does not exist", sep=""))
}


chembl.bioactives <- function(id, verbose=TRUE) {
  
  if(.chembltype(id)=="compound")
    return(chembl.compound.bioactives(id, verbose=verbose))
  
  if(.chembltype(id)=="target")
    return(chembl.target.bioactives(id, verbose=verbose))
  
}

##  List of all bioactivity records in ChEMBLdb for a given compound ChEMBLID 
chembl.compound.bioactives <- function(id, verbose=TRUE) {
  oopsa <- requireNamespace("XML", quietly = TRUE)
  oopsb <- requireNamespace("RCurl", quietly = TRUE)
  if(!all(c(oopsa, oopsb)))
    stop("Please install the XML and RCurl package from CRAN")
  
  if(is.compound(id))
    id <- id$chemblId
  
  url <- 'https://www.ebi.ac.uk/chemblws/compounds/'
  url <- sprintf('%s%s%s', url, id, '/bioactivities' )

  if(verbose)
    cat("  Looking up URL:", url, "\n")
  
  h <- RCurl::getCurlHandle()
  d <- RCurl::getURL(url, curl=h)
  status <- RCurl::getCurlInfo(h)$response.code
  ctype <- RCurl::getCurlInfo(h)$content.type
  rm(h)
  
  if (status == 200) {
    xml <- XML::xmlParse(d)
    data <- XML::xmlToDataFrame(XML::getNodeSet(xml, "/list/bioactivity"), stringsAsFactors=FALSE)

    if(verbose)
        cat(" ", nrow(data), " CHEMBL entries succesfully read \n")
    
    class(data) <- c(class(data), "chembl", "bioactives")
    return(data)
  } else {
    message(d)
    return(NULL)
  }
}


## List of all bioactivity records in ChEMBLdb for a given target ChEMBLID 
chembl.target.bioactives <- function(id, verbose=TRUE) {
  oopsa <- requireNamespace("XML", quietly = TRUE)
  oopsb <- requireNamespace("RCurl", quietly = TRUE)
  if(!all(c(oopsa, oopsb)))
    stop("Please install the XML and RCurl package from CRAN")
  
  url <- 'https://www.ebi.ac.uk/chemblws/targets/'
  url <- sprintf('%s%s%s', url, id, '/bioactivities' )

  if(verbose)
    cat("  Looking up URL:", url, "\n")
  
  h <- RCurl::getCurlHandle()
  d <- RCurl::getURL(url, curl=h)
  status <- RCurl::getCurlInfo(h)$response.code
  ctype <- RCurl::getCurlInfo(h)$content.type
  rm(h)
  
  if (status == 200) {
    xml <- XML::xmlParse(d)
    data <- XML::xmlToDataFrame(XML::getNodeSet(xml, "/list/bioactivity"),
                                stringsAsFactors=FALSE)

    if(verbose)
        cat(" ", nrow(data), " CHEMBL compounds(s) succesfully read \n")
    
    return(data)
  } else {
    return(d)
  }
}



