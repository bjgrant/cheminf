\name{tanimoto}
\alias{tanimoto}
\alias{tanimoto.character}
\alias{tanimoto.default}
\alias{tanimoto.sdf}
\alias{tanimoto.sdfs}
\title{ Tanimoto Coefficient Calculation with OpenBabel }
\description{
  Calculates the Tanimoto Coefficient.
}
\usage{
tanimoto(\dots)

\method{tanimoto}{character}(\dots)

\method{tanimoto}{default}(a, b=NULL, exefile="babel", verbose=FALSE,
ncore=1, \dots)

\method{tanimoto}{sdf}(a, b, \dots)

\method{tanimoto}{sdfs}(a, \dots)
}
\arguments{
  \item{a}{ an sdf(s) object as otained from \code{read.sdf}. }
  \item{b}{ an sdf(s) object as otained from \code{read.sdf}. }
  \item{exefile}{ file path to the \sQuote{BABEL} program on your system (i.e.
    how is \sQuote{BABEL} invoked).  }
  \item{verbose}{ logical, if TRUE \sQuote{BABEL} warning and error
    messages are printed. }
  \item{ncore}{ number of cores to use. }
  \item{\dots}{ arguments passed to and from functions. }
}
\details{
  This function calls the \sQuote{OpenBabel} program, to perform the
  calculation of the tanimoto coefficients. OpenBabel must therefore be
  installed on your system and in the search path for executables.

  Currently very slow for large SDF files.
}
\value{
  a numeric matrix of pairwise tanimoto coefficients.
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
   
  Full details of the \sQuote{OpenBabel} program, along with download and
  installation instructions can be obtained from:\cr
  \url{http://www.openbabel.org}.
}
\author{ Lars Skjaerven }
\note{
  A system call is made to the \sQuote{BABEL} program, which must be
  installed on your system and in the search path for executables.
}
\seealso{ 
  \code{\link{read.sdf}}, \code{\link{draw.sdf}}
}
\examples{
\dontrun{
## Read SDF file
f <- system.file("examples/TMP_analogues.sdf.gz", package="cheminf")
sdfs <- read.sdf(f)

## Calculate all pairwise Tanimoto Coefficients
tan <- tanimoto(sdfs)

## Plot similarity heatmap
heatmap(1-tan)

## Tanimoto only to first SDF
tanimoto(sdfs[[1]], sdfs)

## Tanimoto between two SDFs
tanimoto(sdfs[[1]], sdfs[[2]])
}
}
\keyword{ analysis }
